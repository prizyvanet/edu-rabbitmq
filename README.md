# Обущающий стенд RabbitMQ #

Шатание безопасности кролика и многое другое.

### Зачем? ###
* Конфигрурируем, проверяем
* Ковыряем

Проблематика вопроса стоит так:
__Любой из `webmqttuser`-клиентов создаёт очереди, но злоумышенник может подписаться на `#` и увидеть чужую переписку.__


### Структура ###
* `svc-rabbitmq` - подопытный кролик
* `svc-nginx` - веб-сайт с эмулятором лив-чата
* `svc-hack` - скипты атакующей стороны
* `run-local.sh` - удаляет контейнеры и немного чистит папку

##### Веб-сервер #####
`www-html` - файлы "сайта":  
* `index.html` - пример сайта. Там не работают кнопки, кроме зелёной "выход  
* `index.js` - основная логика, использует Paho MQTT поверх WS `mqttws31.js`  
* `main.js` - не используется, хотел эмулировать вкладки на сессиях веб-сервера. Забросил идею.  
* `pahomqtt.js` - не используется. В index.js закомменчен пример, как соединиться на чистом MQTT, не отлаживал.    
  
### Как установить ###
##### Запуск #####
```
docker-compose up --build
```
* после запуска открываем дашборд кролика http://localhost:15672 (admin, admin)
* открываем сайт http://localhost:8001 - пишем что-то в чат
* атакуем в другом терминале:
```
docker-compose exec svc-hack python main.py -u webmqttuser -p 9883 -t myclientid_42 -c myclientid_42
```
Если не указывать имя топика, будет по умолчанию решётка `#`.  
Доступна справка по параметрам `main.py -h`.


##### Конфигурация #####
* порты наружу в docker-compose
* внутрение порты в докерфайлах `./svc-*/DockerFile`
* веб-сервер настраивается в `./svc-nginx/nginx-conf.d/`
* кролик настраивается в `./svc-rabbitmq/conf/`

##### Особенности портов #####
Порты развесил нестандартно. Два на хосты MQTT.
```
    ports:
      - 5672:5672    # AMQP protocol
      - 15672:15672  # http management dashboard
      - 9883:9883    # mqtt for mqttuser
      - 9884:9884    # mqtt for load balancing for mqttadmin, but mqttuser not allowed
      - 25675:25675  # http/web-mqtt Plugin over tcp without SSL
```

MQTT работает по TCP (и даже с TSL) - но он не знает про виртуальные хосты, поэтому хосты маппят на порты.  
В моём случае 9883 и 9884 - как раз 2 примера таких хостов.   
Я считаю - что один виртуальный хост - для девайсов пользователей, другой - для админов типа Меса,  
третий - для шины логики бизнес-событий МПК (амо-синхра и т.д.).  
Магия разводки портов происходит в `./svc-rabbitmq/conf/definitions.json`:
```
  "global_parameters": [
    {
      "name": "mqtt_port_to_vhost_mapping",
      "value": {
        "9883": "\/vhost-mqtt-device",
        "9884": "\/vhost-mqtt-admin",
        "5673": "\/vhost-inner"
      }
    }
  ],
```

##### Особенности конфигурации #####
* Кролик генерирует свой эрланг-конфиг на основе rabbitmq.conf 
* Пользователь не создаётся (не получится!) из `rabbitmq.conf` и переменных окружения `RABBITMQ_DEFAULT_USER`, потому что создаётся в `definitions.json`
* Все `definitions.json` можно на горячую экспортировать/импортировать через дашборд http://localhost:15672 в разделе Overview 
* А еще все изменения из `definitions.json` можно воспроизвести в контейнере командами `rabbitmqctl`
* Не забываем давать права на виртуальные хосты пользователям в `definitions.json` (админ у нас просто пользак)

##### Установка ограничений #####
Ограничение по топику сделано только для публичного пользоватея `webmqttuser`:
```
  "topic_permissions": [
    {
      "user": "webmqttuser",
      "vhost": "/vhost-mqtt-device",
      "exchange": "mqtt.topic",
      "write": ".*{client_id}.*",
      "read": ".*{client_id}.*"
    }
  ],
```
Ограничения самого пользователя на доступ к виртуальному хосту и на ресурсы внутри хоста:  
```
  "permissions": [
    {
      "user": "webmqttuser",
      "vhost": "\/vhost-mqtt-device",
      "configure": "^(mqtt-subscription-myclientid_\\d*qos\\d)$",
      "write": "^(mqtt.topic|mqtt-subscription-myclientid_\\d*qos\\d)$",
      "read": "^(mqtt.topic|mqtt-subscription-myclientid_\\d*qos\\d)$"
    },
```
Право конфигурирования означает в т.ч.удаление ресурса, поэтому пользователь конфигурирует только очередь своего типа, при `cleanSession: true` может аукнуться, ведь нужно удалить существующую очередь.

Странная неочевидная хрень: пришлось дать доступ даже на чтение к ресурсам `mqtt.topic` (это мой кастомный эксчендж, аналог `amq.topic`), 
сделано это потому, что биндинг происходит через эксчендж, а для этого надо что-то читать.

Если использовать при соединении на клиенте `cleanSession: true` - может не хватить прав на удаление очереди, если убрать `mqtt.topic` из допустимых ресурсов,  поэтому я вынес её в конфигурацию и на всякий случай дал право на запись.   
Для исследования этого вопроса - смотрите логи кролика с `log-level=debug` (сейчас он и установлен).   

##### Проблемы установки топик-ограничений #####
Сейчас создан эксчендж `mqtt.topic`, но я как-то плохо сконфигурировал (не дал админу прав),
поэтому в дашборде http://localhost:15672 при конфигурировании топика её просто нельзя выбрать.
Предлагаю не городить огород, использовать вместо неё стандартную `amq.topic`.
```
    {
      "name": "mqtt.topic",
      "vhost": "\/vhost-mqtt-device",
      "type": "topic",
      "durable": true,
      "auto_delete": false,
      "internal": false,
      "arguments": {}
    }
```


### Что можно контрибьютить ###
* хочется поднять чистый MQTT-клиент без вебсокета (код лежит в файлах веб-сервера nginx)
* хочется немного полезных примеров на ноде / тайпскрипте, а то без гайдов тяжко осваивать MQTT
* приветствуются идеи скриптов, эмулирующих действия атакующего



### Полезные ссылки ###
* https://github.com/eclipse/paho.mqtt.javascript
* https://devops.datenkollektiv.de/creating-a-custom-rabbitmq-container-with-preconfigured-queues.html
* https://stackoverflow.com/questions/30747469/how-to-add-initial-users-when-starting-a-rabbitmq-docker-container
* https://www.rabbitmq.com/vhosts.html
* https://www.rabbitmq.com/mqtt.html
* https://www.rabbitmq.com/parameters.html
* https://www.rabbitmq.com/configure.html#config-file
* https://www.rabbitmq.com/vhosts.html
* https://www.rabbitmq.com/configure.html
* https://www.rabbitmq.com/configure.html#config-file
* https://github.com/rabbitmq/rabbitmq-server/blob/master/deps/rabbit/docs/rabbitmq.conf.example
* https://github.com/rabbitmq/rabbitmq-server/blob/master/deps/rabbit/docs/advanced.config.example