// const client = new Paho.Client("locahost",15675,"WebBrowser");
//   client.onConnectionLost = response=>console.log("lostMQTTConnection: " +(response.errorCode !== 0 ? response.errorMessage : "Unknown MQTT Error" ));
//   client.onMessageArrived = message=>console.log(message.payloadString);
//   client.connect({
//     onSuccess:_=>client.subscribe("test"),
//     useSSL:false,
//     userName:"mqttuser",
//     password:"mqttuser",
//   });




var wsbroker = location.hostname;  //mqtt websocket enabled broker
var wsport = 25675; // port for above
var has_had_focus = false;
var client_id = "myclientid_" + parseInt(Math.random() * 100, 10)
var subscribe_queue = client_id
var userName = "webmqttuser"
var password = "webmqttuser"

var pipe = function (el_name, send) {
    var div = $(el_name + ' div');
    var inp = $(el_name + ' input');
    var form = $(el_name + ' form');

    var print = function (m, p) {
        p = (p === undefined) ? '' : JSON.stringify(p);
        div.append($("<code>").text(m + ' ' + p));
        div.scrollTop(div.scrollTop() + 10000);
    };

    if (send) {
        form.submit(function () {
            send(inp.val());
            inp.val('');
            return false;
        });
    }
    return print;
};

var print_first = pipe('#first', function (data) {
    message = new Paho.MQTT.Message(data);
    message.destinationName = subscribe_queue;
    debug("SEND ON " + message.destinationName + " PAYLOAD " + data);
    client.send(message);
});

var debug = pipe('#second');

var client = new Paho.MQTT.Client(wsbroker, wsport, "/ws", client_id);

client.onConnectionLost = function (responseObject) {
    debug(client_id + ": CONNECTION LOST - " + responseObject.errorMessage);
};

client.onMessageArrived = function (message) {
    debug(client_id + ": RECEIVE ON " + message.destinationName + " PAYLOAD " + message.payloadString);
    print_first(message.payloadString);
};

var options = {
    timeout: 3,
    keepAliveInterval: 30,
    cleanSession: false, // if false operation queue.declare caused a channel exception not_found: no queue 'mqtt-subscription-myclientid_43qos1'
    userName: userName,
    password: password,
    hosts: ["localhost"],
    ports: [wsport],
    onSuccess: function () {
        debug("CONNECTION SUCCESS: " + client_id);
        client.subscribe(subscribe_queue, {qos: 1, onFailure: function (message) {
                debug("SUBSCRIBE FAILURE - " + message.errorMessage);
            },
            onSuccess: function (r) {
                debug("SUBSCRIBE SUSSESS" + r.toString());
                console.log(r)
            }
        });
    },
    onFailure: function (message) {
        debug("CONNECTION FAILURE - " + message.errorMessage);
    }
};

if (location.protocol == "https:") {
    options.useSSL = true;
}

debug("CONNECT TO " + wsbroker + ":" + wsport + "-->" + options.toString());
client.connect(options);

$('#first input').focus(function () {
    if (!has_had_focus) {
        has_had_focus = true;
        $(this).val("");
    }
});

$('#id_btn_diconnect').click(function(){
    if (!client.isConnected()) {
        debug("Already disconnected - " + client.toString());
        return false;
    }
    client.unsubscribe(subscribe_queue);
    client.disconnect();
    return false;
})