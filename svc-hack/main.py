import paho.mqtt.client as mqtt
import sys
import argparse


def on_connect(client, userdata, flags, rc):
    print("Connected with result code", rc)
    client.subscribe(topic)


def on_message(client, userdata, msg):
    print(msg.topic, msg.payload)


def get_args():
    print("example: python main.py -u webmqttuser -p 9883 -t listen.topic -c myclientid_72")
    parser = argparse.ArgumentParser(description='MQTT connector')
    parser.add_argument('-u', '--username', dest="username", help="one of <webmqttuser, mqttuser, mqttadmin, admin>")
    parser.add_argument('-p', '--port', dest="port", type=int,
                        help="port: 9883 (mqtt), 9884 (mqtt), 25675 (web-mqtt) not allowed")
    parser.add_argument('-c', '--client_id', dest="client_id", help="any str or like myclientid_42")
    parser.add_argument('-t', '--topic', dest="topic", default="#",
                        help="Default # (# not allowed to pass) to subscribe, or like myclientid_42")

    args = parser.parse_args()
    return args.username, int(args.port), args.topic, args.client_id


if __name__ == "__main__":
    username, port, topic, client_id = get_args()
    client = mqtt.Client(client_id=client_id,
                         clean_session=False,
                         userdata=None,
                         protocol=mqtt.MQTTv311,
                         transport="tcp")
    client.on_connect = on_connect
    client.on_message = on_message
    print("Connecting...")
    client.username_pw_set(username=username, password=username)
    client.connect("svc-rabbitmq", port, 10)  # works
    print(client)
    client.loop_forever()
