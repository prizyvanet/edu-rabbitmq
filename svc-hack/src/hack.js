import * as mqtt from 'async-mqtt';
import {v4 as uuid} from 'uuid';
(async () => {
	const m = await mqtt.connectAsync('mqtts://rabbitmq-amq.mpk-prod.ru/ws', {
		clientId: uuid(),
	});
	m.on('message', (topic, payload) => {
		console.log(topic, payload.toString());
	});
	await m.subscribe(`#`);
})();