import pika

creds = pika.credentials.PlainCredentials("webmqttuser", "webmqttuser")

conn_params = pika.ConnectionParameters(
    host="localhost",
    port=9883,
    virtual_host="/vhost-mqtt-device",
    credentials=creds,
    retry_delay=1
)
connection = pika.BlockingConnection(conn_params)
print(connection)
channel = connection.channel()
print(channel)
d = channel.queue_declare(queue='mqtt-subscription-myclientid_11qos1')
print(d)

p = channel.basic_publish(exchange="mqtt.topic",
                      routing_key='test2',
		      #durable=False,
                      body='Hello World!')
print(p)

print("closing..")
connection.close()
print("Done")