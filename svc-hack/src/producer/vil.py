import pika
import requests
import sys

host = "localhost"

def get_queues(host="localhost", port=5672, user="guest", passwd="guest", virtual_host="/"):
    url = 'http://%s:%s/api/queues/%s' % (host, port, virtual_host or '')
    response = requests.get(url, auth=(user, passwd))
    return response.json()

queues = get_queues(host)

def get_on_message(queue):
    def on_message(channel, method_frame, header_frame, body):
        print("message from", queue)
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)
    return on_message

# connection = pika.BlockingConnection(pika.ConnectionParameters(host))
# channel = connection.channel()

for queue in queues:
    print(channel.is_open)
    try:
        channel.basic_consume(get_on_message(queue["name"]), queue["name"])
        print("queue added",queue["name"])
    except Exception as e:
        print("queue failed",queue["name"])
        sys.exit()

#try:
#    channel.start_consuming()
#except KeyboardInterrupt:
#    channel.stop_consuming()
#connection.close()