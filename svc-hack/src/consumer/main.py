import pika
# https://codeburst.io/using-rabbitmq-for-microservices-communication-on-docker-a43840401819

creds = pika.credentials.PlainCredentials("webmqttuser", "webmqttuser")

conn_params = pika.ConnectionParameters(
    host="localhost",
    port=9883,
    virtual_host="/vhost-mqtt-device",
    credentials=creds,
    retry_delay=1
)


connection = pika.BlockingConnection(conn_params)
channel = connection.channel()

channel.queue_declare(queue='mqtt-subscription-myclientid_22qos1')


def callback(ch, method, properties, body):
    print(" [x] Received %r" % (body,))


channel.basic_consume(queue='mqtt-subscription-myclientid_22qos1', on_message_callback=callback, auto_ack=True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
