import paho.mqtt.client as mqtt

def on_connect(client, userdata, flags, rc):
    print("Connected with result code", rc)
    client.subscribe("listen")

def on_message(client, userdata, msg):
    print(msg.topic, msg.payload)


client = mqtt.Client(client_id="", clean_session=True, userdata=None, protocol=mqtt.MQTTv311, transport="tcp")
client.on_connect = on_connect
client.on_message = on_message

# client.tls_set(False)  # <--- even without arguments

print("Connecting...")

client.username_pw_set(username="mqttuser", password="mqttuser") # works
client.connect("localhost", 9883, 10) # works

# client.username_pw_set(username="webmqttuser", password="webmqttuser") # works
# client.connect("localhost", 9883, 10) # works


# client.username_pw_set(username="admin", password="admin") # не будет работать, не разрешён доступ через MQTT на /vhost-mqtt-device
# client.connect("localhost", 9883, 10) 

# client.username_pw_set(username="mqttadmin", password="mqttadmin")
# client.connect("localhost", 9883, 10)
# client.connect("localhost", 9884, 10)


print(client)
client.loop_forever()